import Vue from 'vue'
import App from './App.vue'
import myComponet from '../../dict/my-component.esm'
Vue.use(myComponet)

new Vue({
    el: '#app',
    render: (h) => h(App)
})
