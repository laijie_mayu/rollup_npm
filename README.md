## Quick Start
### 安装

 ```js
    npm install sdk-for-leaning --save
 ```
 或者

 ```js
    yarn add sdk-for-leaning
 ```

### 使用

#### 1、按需引用
 ```js
    import { Header } from "sdk-for-leaning";
    components: { Header },
 ```

 ```html
  <Header><Header/>
 ```

#### 2、全局引用

 ```js 
    import  Header  from "sdk-for-leaning";
    Vue.use(Header)
 ```

 ```html
   <Header><Header/>
 ```



