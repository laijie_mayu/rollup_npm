import MyComponet from './componets/index'

const components = [MyComponet]
const install = function (Vue) {
    components.forEach(component => {
        Vue.component(component.name, component)
    })
}
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue)
}
export { MyComponet as Header }
export default install
