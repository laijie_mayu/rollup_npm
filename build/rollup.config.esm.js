import base, { OUTPUT_DIT, LIBRARY_NAME, myPackage } from './rollup.config.base'
import path from 'path'

const config = {
    ...base,
    output: {
        name: LIBRARY_NAME,
        file: path.join(OUTPUT_DIT, myPackage.module),
        format: 'esm',
    },
}
export default config
